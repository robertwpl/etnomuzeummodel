﻿using UnityEngine;
using System.Collections;

public class MlotekScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Transform ori = transform.FindChild("MlotekZespol");
        GameObject copy = Instantiate(ori, ori.position, ori.rotation) as GameObject;
        copy.transform.localScale = ori.transform.localScale;
        copy.transform.parent = transform;
        copy.transform.position = new Vector3(copy.transform.position.x+.0002f, copy.transform.position.y, copy.transform.position.z);
	}
	
	
}
