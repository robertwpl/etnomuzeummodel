﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoCtrl : MonoBehaviour {
    
     Text infoText;
    [SerializeField]
    GameObject infoDisplay;
     bool pressed;
     Color baseColor;
     [SerializeField]
     float fadeSpeedSeconds;
	 
	void Awake () {
        infoDisplay.SetActive(true);
        infoText = infoDisplay.GetComponentInChildren<Text>();
        baseColor = infoText.color;
        infoText.color = Color.clear;
	}

    public void OnMouseDown()
    {
        pressed = true;
    }

    public void OnMouseUp()
    {
        pressed = false;
    }
	
	
	void Update () {
        if (pressed)
        {
            infoText.color = Color.Lerp(infoText.color, baseColor, fadeSpeedSeconds * Time.deltaTime);
        }
        else
        {
            infoText.color = Color.Lerp(infoText.color, Color.clear, fadeSpeedSeconds * Time.deltaTime);
        }
	}
}
