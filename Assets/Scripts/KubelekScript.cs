﻿using UnityEngine;
using System.Collections;

public class KubelekScript : MonoBehaviour
{
    private bool isDown;
    private bool isZero = true;
    public bool wasDown;
    private float startRotation;
    public ParticleSystem LenSypanyParticles;
    public ActiveZone zone;
    [SerializeField]
    private float limitRotation = 135f;
    private float limitAdd = 10f;
    void Start()
    {
        if (!object.ReferenceEquals(zone, null))
        {
            zone.pointerBegin += OnMouseDown;
            zone.pointerEnd += OnMouseUp;
        }

        isDown = false;
        transform.rotation = Quaternion.Euler(270f,0f,0f);
        startRotation = transform.rotation.eulerAngles.x;
        LenSypanyParticles.Stop();
    }

    public void OnMouseDown()
    {
        isDown = true;
    }

    public void OnMouseUp()
    {
        isDown = false;
    }

    void Update()
    {
        if (!isZero && transform.rotation.eulerAngles.x < startRotation + limitRotation / 5)
            isZero = true;
        Quaternion target = Quaternion.Euler(isDown ? startRotation + limitRotation + limitAdd : startRotation, 0f, 0f);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime);
        if (isZero && transform.rotation.eulerAngles.x > startRotation + limitRotation && LenSypanyParticles.isStopped)
        {
            isZero = false;
            wasDown = true;
            LenSypanyParticles.Play();
        }
        else if (LenSypanyParticles.isPlaying && transform.rotation.eulerAngles.x < startRotation + limitRotation)
        {
            LenSypanyParticles.Stop();
        }
    }
}
