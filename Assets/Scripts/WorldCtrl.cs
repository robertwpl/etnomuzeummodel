﻿using UnityEngine;
using System.Collections;

public class WorldCtrl : MonoBehaviour {
    Vector3 baseRot;
    public void Start()
    {
        baseRot = transform.rotation.eulerAngles;
    }


    public void rotate(float amount)
    {
        transform.rotation = Quaternion.Euler(baseRot.x, baseRot.y+360f*amount-180f, baseRot.z);
    }
}
