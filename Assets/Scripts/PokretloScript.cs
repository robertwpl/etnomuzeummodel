﻿using UnityEngine;
using System.Collections;

public class PokretloScript : MonoBehaviour
{
		public Transform Zasowka;
		public ParticleSystem LenParticles;
		public OsCtrl oska;
		public KubelekScript kubelek;
		private bool isDown;
		private float startRotation;
		private float startZasowkaRotation;
		public ActiveZone zone;
        public bool zsypOpened = false;
		void Start ()
		{
            if (!object.ReferenceEquals(zone, null))
            {
                zone.pointerBegin += OnMouseDown;
                zone.pointerEnd += OnMouseUp;
            }
				LenParticles.Stop ();
				startRotation = transform.rotation.eulerAngles.z;
				startZasowkaRotation = Zasowka.rotation.eulerAngles.x;
		}

		public void OnMouseDown ()
		{
				isDown = true;
		}
	
		public void OnMouseUp ()
		{
				isDown = false;
		}

		void Update ()
		{
				Quaternion target = Quaternion.Euler (0f, 0f, isDown ? startRotation + 150f : startRotation);
				transform.rotation = Quaternion.Slerp (transform.rotation, target, Time.deltaTime);
				Zasowka.transform.rotation = Quaternion.Slerp (Zasowka.transform.rotation, Quaternion.Euler (startZasowkaRotation + (target.eulerAngles.z - startRotation) / 2, 0f, 0f), Time.deltaTime);
				if (target.eulerAngles.z > startRotation + 50f && LenParticles.isStopped && oska.rot > 0 && kubelek.wasDown) {
						LenParticles.Play ();
                        zsypOpened = true;
				} else if (LenParticles.isPlaying && target.eulerAngles.z < startRotation + 50f) {
						LenParticles.Stop ();
                        zsypOpened = false;
						//Debug.Log ("stoping");
				}
		}
}
