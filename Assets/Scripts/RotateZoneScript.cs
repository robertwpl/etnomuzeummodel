﻿using UnityEngine;
using System.Collections;

public class RotateZoneScript : MonoBehaviour
{
		public delegate void Rotated (float rot);
		public Rotated rotated;
		private bool isDown;
		// Use this for initialization
		void Start ()
		{
				var recognizer = new TKOneFingerRotationRecognizer ();
				recognizer.targetPosition = Camera.main.WorldToScreenPoint (transform.position);
				recognizer.gestureRecognizedEvent += ( r ) =>
				{
						if (isDown)
								rotated (recognizer.deltaRotation);
				};
				TouchKit.addGestureRecognizer (recognizer);
	
		}

		void OnMouseDown ()
		{
				isDown = true;
		}
	
		void OnMouseUp ()
		{
				isDown = false;
		}
	

}
