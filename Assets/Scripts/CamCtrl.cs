﻿using UnityEngine;
using System.Collections;

public class CamCtrl : MonoBehaviour
{
		public Camera[] cameras;
		private int idx = 0;
		void OnGUI ()
		{
				float size = 80f;
				if (cameras.Length > 0 && GUI.Button (new Rect (Screen.width - size, Screen.height - 23f, size - 3f, 20f), "Next Cam")) {
						idx = idx == cameras.Length - 1 ? 0 : ++idx;
						for (int i=0; i<cameras.Length; i++) {
								cameras [i].enabled = i == idx;
						}	
				}
		}
}
