﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
		public Texture textTex;
		public Texture logoTex;
		public Texture2D btnTex;
		public Texture2D bgTex;
		public Texture2D copyIcons;
		public Texture2D logotypesTex;
		public Texture2D aboutTex;
		public Texture2D infoTex;
		public Texture2D infoBtnTex;
		public Texture2D closeBtnTex;
		public Texture2D tryjerText;
		public Texture2D reloadTex;
		enum state
		{
				ZERO,
				MAIN,
				ABOUT,
				OVER
	}
		;
		state currentState = state.ZERO;
		void Awake ()
		{
				//DontDestroyOnLoad (this);
		}
		void OnGUI ()
		{
				float hPart = .1f;
				float padding = Screen.width * .05f;
				float sc = Screen.width / 1024f;
				if (currentState == state.ZERO) {
						Texture2D t = new Texture2D (1, 1);
						t.SetPixel (0, 0, new Color32 (254, 252, 241, 255));
						t.Apply ();
						GUI.skin.box.normal.background = t;
						GUI.Box (new Rect (0f, 0f, Screen.width, Screen.height), GUIContent.none);
						GUI.skin.button.normal.background = btnTex;
						GUI.skin.button.hover.background = btnTex;
						GUI.skin.button.active.background = btnTex;
						if (GUI.Button (new Rect (Screen.width / 2 - btnTex.width / 2, Screen.height - Screen.height * hPart - btnTex.height / 2, btnTex.width, btnTex.height), "")) {
								currentState = state.MAIN;
						}
						GUI.DrawTexture (new Rect (Screen.width / 2 - textTex.width * sc / 2, Screen.height / 2 - textTex.height * sc / 2, textTex.width * sc, textTex.height * sc), textTex, ScaleMode.ScaleToFit, true);
						GUI.DrawTexture (new Rect (Screen.width / 2 - logoTex.width * sc / 2, Screen.height * sc / 4 - logoTex.height * sc / 2, logoTex.width * sc, logoTex.height * sc), logoTex, ScaleMode.ScaleToFit);
				}
				if (currentState == state.MAIN) {
						GUI.DrawTexture (new Rect (0, Screen.height * (1 - hPart), Screen.width, Screen.height * hPart), bgTex, ScaleMode.StretchToFill);
						GUI.DrawTexture (new Rect (padding, Screen.height - Screen.height * hPart / 2 - tryjerText.height / 2, tryjerText.width, tryjerText.height), tryjerText, ScaleMode.StretchToFill);
						GUI.skin.button.normal.background = infoBtnTex;
						GUI.skin.button.hover.background = infoBtnTex;
						GUI.skin.button.active.background = infoBtnTex;			
						if (GUI.Button (new Rect (Screen.width - 4 * padding, Screen.height - Screen.height * hPart / 2 - infoBtnTex.height / 2, infoBtnTex.width, infoBtnTex.height), "")) {
								currentState = state.ABOUT;
						}
						GUI.skin.button.normal.background = reloadTex;
						GUI.skin.button.hover.background = reloadTex;
						GUI.skin.button.active.background = reloadTex;			
						if (GUI.Button (new Rect (Screen.width - 3 * padding, Screen.height - Screen.height * hPart / 2 - reloadTex.height / 2, reloadTex.width, reloadTex.height), "")) {
								Application.LoadLevel ("scene3");
						}

				}
				if (currentState == state.ABOUT) { 
						Texture2D t = new Texture2D (1, 1);
						t.SetPixel (0, 0, new Color32 (255, 255, 255, 255));
						t.Apply ();
						GUI.skin.box.normal.background = t;
						GUI.Box (new Rect (0f, 0f, Screen.width, Screen.height), GUIContent.none);
						float rel = 7f / 12f;			
						GUI.DrawTexture (new Rect (0, Screen.height * rel, Screen.width, Screen.height * (1 - rel)), bgTex, ScaleMode.StretchToFill);
						GUI.DrawTexture (new Rect (Screen.width / 2 - logotypesTex.width / 2 * sc, padding, logotypesTex.width * sc, logotypesTex.height * sc), logotypesTex, ScaleMode.ScaleToFit);
						GUI.DrawTexture (new Rect (padding, Screen.height * rel + padding, aboutTex.width * sc, aboutTex.height * sc), aboutTex, ScaleMode.StretchToFill);
						GUI.DrawTexture (new Rect (padding, Screen.height - Screen.height * hPart / 2 - copyIcons.height / 2, copyIcons.width, copyIcons.height), copyIcons, ScaleMode.ScaleToFit);
						GUI.skin.button.normal.background = closeBtnTex;
						GUI.skin.button.hover.background = closeBtnTex;
						GUI.skin.button.active.background = closeBtnTex;			
						if (GUI.Button (new Rect (Screen.width - padding - closeBtnTex.width, Screen.height - Screen.height * hPart / 2 - infoBtnTex.height / 2, infoBtnTex.width, infoBtnTex.height), "")) {
								currentState = state.MAIN;
						}
					
				}
		}
}
