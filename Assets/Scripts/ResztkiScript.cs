﻿using UnityEngine;
using System.Collections;

public class ResztkiScript : MonoBehaviour {

    [SerializeField]
    private ParticleSystem ziarna;
    [SerializeField]
    private ParticleSystem plewy;
    [SerializeField]
    private PokretloScript pokretlo;
    private bool latched = false;
    private float playTime;
    [SerializeField]
    GameObject ziarnaZone;
    [SerializeField]
    GameObject plewyZone;

    public void Start()
    {
        ziarnaZone.SetActive(false);
        plewyZone.SetActive(false);
    }
	
	void Update () {
        if (pokretlo.zsypOpened && !latched)
        {
            latched = true;
            ziarna.Play();
            plewy.Play();
            playTime = Time.time;
        }
        else if (!pokretlo.zsypOpened && latched)
        {
            latched = false;
            if (Time.time > playTime + 5f)
            {
            ziarna.Pause();
            plewy.Pause();
            ziarnaZone.SetActive(true);
            plewyZone.SetActive(true);
            }
            else
            {
            ziarna.Stop();
            plewy.Stop();
            }
            
        }
	}
}
