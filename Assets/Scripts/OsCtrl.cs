﻿using UnityEngine;
using System.Collections;

public class OsCtrl : MonoBehaviour
{
		public float speed = 50f;
		public float rot = 0f;
		public RotateZoneScript zoneScript;
		void Start ()
		{
			if(!object.ReferenceEquals(zoneScript,null))
            zoneScript.rotated += (r) => {
						rot = r;
				};
		}
        public void OnMouseDown()
        {
            rot=3f;
        }

        public void OnMouseUp()
        {
            //rot=0f;
        }
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.LeftArrow))
						rot -= .2f;
				else if (Input.GetKeyDown (KeyCode.RightArrow))
						rot += .2f;
				var cur = new Vector3 (0f, 0f, Mathf.Lerp (0f, Mathf.Abs (rot), 2000f));
				transform.Rotate (cur * speed * Time.deltaTime);
		}

}
